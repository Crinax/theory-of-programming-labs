cmake_minimum_required (VERSION 2.8.12)
project (Laboratory1)

file (GLOB_RECURSE SOURCE_FILES "${PROJECT_SOURCE_DIR}/src/*.cpp")

set (CMAKE_CXX_STANDARD 20)

add_subdirectory ("${PROJECT_SOURCE_DIR}/template")
add_executable (Laboratory1 ${SOURCE_FILES})
target_link_libraries (Laboratory1 PUBLIC LaboratoryTemplate)
target_include_directories (Laboratory1 PUBLIC "${PROJECT_SOURCE_DIR}/template/src")
