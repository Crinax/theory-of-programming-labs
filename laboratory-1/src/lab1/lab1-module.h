#pragma once

#include <core/app.h>
#include <core/ui.h>

class Lab1Module : public application::Module {
	public:
		void load_dependencies(ui::WindowManager*);
};
