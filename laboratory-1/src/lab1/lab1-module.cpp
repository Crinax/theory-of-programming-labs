#include <functional>
#include <core/ui.h>
#include "lab1-module.h"
#include "./pages/main-page.h"
#include "../constants/lab1_constants.h"

void Lab1Module::load_dependencies(ui::WindowManager* window_manager) {
  Lab1MainPage main_page;

  std::function<bool(ui::WindowManager*)> main_view = std::bind(
    &Lab1MainPage::display,
    main_page,
    std::placeholders::_1
  );

  window_manager->add(pages::lab1::main, main_view);
}

