#pragma once
#include <string>
#include <cmath>
#include <core/ui.h>

class Lab1MainPage {
	public:
		Lab1MainPage();
		
		bool display(ui::WindowManager*);
    std::string make_function_tabulation(double, double, double, double);
    void save_to_file(std::string, std::string);

  private:
    // 9th variant
    std::function<double(double)> fn = [](double x) -> double {
      return (
        (x - std::log(std::pow(x, 2) + 1)) *
        (x - std::sin(3 * x))
      ) / (x + std::pow(3.2, 2 * x - 1)); 
    };
};

