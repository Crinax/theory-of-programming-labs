#include <cmath>
#include <iomanip>
#include <limits>
#include <functional>
#include <string>
#include <fstream>
#include <sstream>
#include <core/ui.h>
#include <components/TextComponent/TextComponent.h>
#include <components/WindowHeader/WindowHeader.h>
#include <components/InputReaderComponent/InputReaderComponent.h>
#include <components/InputComponent/InputComponent.h>
#include "main-page.h"
#include "../../constants/constants.h"
#include "../../headers/lab1-headers.h"

Lab1MainPage::Lab1MainPage() {}

bool Lab1MainPage::display(ui::WindowManager* window_manager) {
  double start, end, step, precision;
  std::string file_name;
  std::function<double(std::string)> parse_double = [](std::string text) -> double {
    return std::stod(text);
  };

  ui::WindowHeader(headers::lab1::base_header).display();

  ui::TextComponent("Enter start of range: ").display();
  ui::InputReaderComponent<double>(start, parse_double).display();

  ui::TextComponent("Enter end of range: ").display();
  ui::InputReaderComponent<double>(end, parse_double).display();

  ui::TextComponent("Enter step of range: ").display();
  ui::InputReaderComponent<double>(step, parse_double).display();

  ui::TextComponent("Enter precision of sum: ").display();
  ui::InputReaderComponent<double>(precision, parse_double).display();

  std::stringstream answer;
  answer << "\nYour range: [" << start << ", " << end << "; " << step << "]\n";

  ui::TextComponent(answer.str()).display();
  ui::TextComponent("\nFunction tabulation\n").display();

  std::string tabulation = this->make_function_tabulation(start, end, step, precision);

  ui::TextComponent(tabulation).display();
  ui::TextComponent("\nSaving to file...").display();

  this->save_to_file(tabulation, "output.txt");

  ui::TextComponent("\nSaved to file output.txt successfully!").display();
  ui::TextComponent("\nPress <Enter> to return to the menu...").display();

  return ui::InputComponent([window_manager](std::string _) -> double {
    window_manager->switch_to(pages::base);
    return false;
  }).display();
}

std::string Lab1MainPage::make_function_tabulation(
  double start,
  double end,
  double step,
  double precision
) {
  std::stringstream result;
  std::function<double(double, double, double)> prec_sum = [](double x, double y, double p) {
    return (x / p + y / p) * p;
  };

  result.precision();

  for (double x = start; x <= end; x = prec_sum(x, step, precision)) {
    result <<
      "f(" <<
      x << 
      ") = " <<
      this->fn(x) <<
      std::endl;
  }

  return result.str();
}

void Lab1MainPage::save_to_file(std::string info, std::string name) {
  std::ofstream file(name);

  file << info;

  file.close();
}
