#include <functional>
#include <vector>
#include <core/ui.h>
#include "lab2-module.h"
#include "./pages/main-page.h"
#include "../constants/lab2_constants.h"
#include "./pages/input-page.h"
#include "./pages/find-max-page.h"
#include "./pages/sort-page.h"

void Lab2Module::load_dependencies(ui::WindowManager* window_manager) {
  Lab2MainPage main_page = { };
  Lab2InputPage input_page = { };
  Lab2FindMaxPage find_max_page = { };
  Lab2SortPage sort_page = { };
  
  std::function<bool(ui::WindowManager*)> main_view = std::bind(
    &Lab2MainPage::display,
    main_page,
    std::placeholders::_1
  );

  std::function<bool(ui::WindowManager*)> input_view = std::bind(
    &Lab2InputPage::display,
    input_page,
    &this->values,
    std::placeholders::_1
  );

  std::function<bool(ui::WindowManager*)> find_max_view = std::bind(
    &Lab2FindMaxPage::display,
    find_max_page,
    &this->values,
    std::placeholders::_1
  );

  std::function<bool(ui::WindowManager*)> sort_view = std::bind(
    &Lab2SortPage::display,
    sort_page,
    &this->values,
    this,
    std::placeholders::_1
  );

  window_manager->add(pages::lab2::main, main_view);
  window_manager->add(pages::lab2::input_values, input_view);
  window_manager->add(pages::lab2::find_max_values, find_max_view);
  window_manager->add(pages::lab2::sort_values, sort_view);
}

