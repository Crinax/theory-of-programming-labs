#pragma once
#include <vector>
#include <core/ui.h>

class Lab2InputPage {
  public:
    Lab2InputPage();

    bool display(std::vector<double>*, ui::WindowManager*);
};
