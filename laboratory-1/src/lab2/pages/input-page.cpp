#include <functional>
#include <string>
#include <vector>
#include <core/ui.h>
#include <components/InputComponent/InputComponent.h>
#include <components/WindowHeader/WindowHeader.h>
#include <components/TextComponent/TextComponent.h>
#include "components/InputReaderComponent/InputReaderComponent.h"
#include "input-page.h"
#include "../../constants/lab2_constants.h"
#include "../../headers/lab2-headers.h"

Lab2InputPage::Lab2InputPage() {};

bool Lab2InputPage::display(std::vector<double>* values, ui::WindowManager* window_manager) {
  bool ask_user = true;
  int index = 1;
  double value;
  std::function<double(std::string)> parse_double = [&ask_user](std::string value) mutable -> double {
    try {
      return std::stod(value);
    } catch(...) {
      ask_user = false;
      return 0;
    }
  };

  values->clear();
  ui::WindowHeader(headers::lab2::input_header).display();
  ui::TextComponent("![Enter any symbol to stop...]\n").display();

  while (ask_user) {
    ui::TextComponent("Enter " + std::to_string(index) + " value: ").display();
    ui::InputReaderComponent<double>(value, parse_double).display();

    if (ask_user != false) {
      values->push_back(value);
      index++;
    }
  }

  ui::TextComponent("\nPress <Enter> to back to menu...").display();
  ui::InputComponent([](std::string _) -> bool { return false; }).display();

  return !window_manager->switch_to(pages::lab2::main);
}
