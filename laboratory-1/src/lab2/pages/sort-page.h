#pragma once
#include <memory>
#include <vector>
#include <core/ui.h>
#include "../../helpers/sort/sort.h"
#include "../lab2-module.h"

class Lab2SortPage {
	public:
		Lab2SortPage();

		bool display(std::vector<double>*, Lab2Module*, ui::WindowManager*);
};
