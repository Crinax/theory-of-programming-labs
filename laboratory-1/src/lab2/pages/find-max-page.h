#pragma once
#include <vector>
#include <core/ui.h>

class Lab2FindMaxPage {
	public:
		Lab2FindMaxPage();

		bool display(std::vector<double>*, ui::WindowManager*);
};
