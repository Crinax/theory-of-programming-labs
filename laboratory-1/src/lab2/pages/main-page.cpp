#include <functional>
#include <core/ui.h>
#include <components/WindowHeader/WindowHeader.h>
#include <components/Menu/Menu.h>
#include "main-page.h"
#include "../../constants/constants.h"
#include "../../constants/lab2_constants.h"
#include "../../headers/lab2-headers.h"

Lab2MainPage::Lab2MainPage() { };

bool Lab2MainPage::display(ui::WindowManager* window_manager) {
  ui::WindowHeader(headers::lab2::base_header).display();
  ui::Menu menu = {};

  menu.add("Enter values", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab2::input_values);
  });

  menu.add("Find max element lower than 2", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab2::find_max_values);
  });

  menu.add("Sort elements", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab2::sort_values);
  });

  menu.add("Back", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::base);
  });

  menu.add("Exit", []() -> bool { return true; });

  return menu.display();
}
