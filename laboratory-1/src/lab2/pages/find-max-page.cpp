#include <string>
#include <core/ui.h>
#include <components/TextComponent/TextComponent.h>
#include <components/InputComponent/InputComponent.h>
#include "components/WindowHeader/WindowHeader.h"
#include "find-max-page.h"
#include "../../constants/lab2_constants.h"
#include "../../headers/lab2-headers.h"

Lab2FindMaxPage::Lab2FindMaxPage() { };

bool Lab2FindMaxPage::display(std::vector<double>* values, ui::WindowManager* window_manager) {
  int values_count = values->size();

  ui::InputComponent user_input_wait([](std::string _) -> bool { return false; });

  ui::WindowHeader(headers::lab2::find_max_header).display();

  if (values_count == 0) {
    ui::TextComponent("You haven't entered the values yet").display();
    ui::TextComponent("\nPress <Enter> to do it...").display();
    user_input_wait.display();

    return !window_manager->switch_to(pages::lab2::input_values);
  }

  double result = values->at(0);

  ui::TextComponent("Max element lower than 2: ").display();

  for (int i = 1; i < values_count; i++) {
    double current_value = values->at(i);

    if (current_value > result && current_value < 2) {
      result = current_value;
    }
  }

  if (result >= 2) {
    ui::TextComponent("no such elements :(").display();
  } else {
    ui::TextComponent(std::to_string(result)).display();
  }

  ui::TextComponent("\n\nPress <Enter> to back to menu...").display();
  user_input_wait.display();

  return !window_manager->switch_to(pages::lab2::main);
}
