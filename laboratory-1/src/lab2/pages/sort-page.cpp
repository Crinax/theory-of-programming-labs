#include <algorithm>
#include <random>
#include <functional>
#include <core/ui.h>
#include <components/WindowHeader/WindowHeader.h>
#include <components/InputComponent/InputComponent.h>
#include <components/Menu/Menu.h>
#include "components/TextComponent/TextComponent.h"
#include "sort-page.h"
#include "../../constants/constants.h"
#include "../../constants/lab2_constants.h"
#include "../../helpers/sort/sort.h"
#include "../lab2-module.h"

Lab2SortPage::Lab2SortPage() { };

bool Lab2SortPage::display(
  std::vector<double>* values,
  Lab2Module* mod,
  ui::WindowManager* window_manager
) {
  ui::WindowHeader("Laboratory 2. Array management. Sorting").display();
  ui::Menu menu = {};
  int values_count = values->size();


  ui::TextComponent("\nValues: [").display();
  for (int index = 0; index < values_count; index++) {
    if (index != 0) {
      ui::TextComponent(", ").display();
    }

    ui::TextComponent(std::to_string(values->at(index))).display();
  }
  ui::TextComponent("]\n\n").display();

  menu.add("Sort in ascending order", [values = std::move(values)]() -> bool {  
    std::sort(values->begin(), values->end(), helpers::sort_direction::asc<double>());
    return false;
  });
  
  menu.add("Sort in descending order", [values = std::move(values)]() mutable -> bool {  
    std::sort(values->begin(), values->end(), helpers::sort_direction::desc<double>());
    return false;
  });
  
  menu.add("Shuffle", [values]() mutable -> bool {
    std::random_device random_device = { };
    auto random_engine = std::default_random_engine { random_device() };
    std::shuffle(values->begin(), values->end(), random_engine);
    return false;
  });
  
  menu.add("Back", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab2::main);
  });

  menu.add("Exit", []() -> bool { return true; });

  return menu.display();
}
