#include <string>

namespace headers {
  namespace lab2 {
    const std::string base_header = "Laboratory 2. Array management";
    const std::string input_header = base_header + ". Input array";
    const std::string find_max_header = base_header + ". Max values lower than 2";
  }
}
