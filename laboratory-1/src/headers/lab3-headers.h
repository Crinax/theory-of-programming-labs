#include <string>

namespace headers {
  namespace lab3 {
    const std::string base_header = "Laboratory 3. Working with structures";
    const std::string file_input_header = base_header + ". Read from file";
    const std::string input_ask_header = base_header + ". Choose input method";
    const std::string keyboard_input_header = base_header + ". Input from keyboard";
    const std::string sorting_ask_header = base_header + ". Choose sorting method";
  }
}
