#pragma once

#include <core/app.h>
#include <core/ui.h>
#include <memory>
#include "../structures/circle.h"

class Lab3Module : public application::Module {
  public:
    void load_dependencies(ui::WindowManager*);

    std::vector<std::unique_ptr<Circle>> circles = { };
};
