#pragma once

#include <core/ui.h>

class Lab3MainPage {
  public:
    Lab3MainPage();

    bool display(ui::WindowManager*);
};
