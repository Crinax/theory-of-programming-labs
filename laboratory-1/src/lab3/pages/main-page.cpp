#include <iostream>
#include <core/ui.h>
#include <components/Menu/Menu.h>
#include <components/WindowHeader/WindowHeader.h>
#include "main-page.h"
#include "../../constants/constants.h"
#include "../../constants/lab3_constants.h"
#include "../../headers/lab3-headers.h"

Lab3MainPage::Lab3MainPage() {}

bool Lab3MainPage::display(ui::WindowManager* window_manager) {
  ui::Menu menu = { };
  ui::WindowHeader(headers::lab3::base_header).display();

  menu.add("Enter circles params", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab3::input_ask);
  });
  
  menu.add("Sort circles", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab3::sorting);
  });

  menu.add("Back", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::base);
  });

  menu.add("Exit", []() -> bool { return true; });

  return menu.display();
}
