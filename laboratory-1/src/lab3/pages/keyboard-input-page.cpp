#include <vector>
#include <memory>
#include <core/ui.h>
#include <components/WindowHeader/WindowHeader.h>
#include "keyboard-input-page.h"
#include "../../structures/circle.h"
#include "../../constants/lab3_constants.h"
#include "../../helpers/sort/sort.h"
#include "../../headers/lab3-headers.h"

Lab3KeyboardInputPage::Lab3KeyboardInputPage() { };

bool Lab3KeyboardInputPage::display(
  std::vector<std::unique_ptr<Circle>>* circles,
  ui::WindowManager* window_manager
) {
  double x, y, radius;
  circles->clear();

  ui::WindowHeader(headers::lab3::keyboard_input_header).display();

  return false;
}
