#pragma once

#include <memory>
#include <vector>
#include <core/ui.h>
#include "../../structures/circle.h"

class Lab3FileInputPage {
  public:
    Lab3FileInputPage();

    bool display(std::vector<std::unique_ptr<Circle>>*, ui::WindowManager*);
};
