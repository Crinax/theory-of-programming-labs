#include <fstream>
#include <memory>
#include <string>
#include <functional>
#include <core/ui.h>
#include <core/console.h>
#include <components/InputReaderComponent/InputReaderComponent.h>
#include <components/InputComponent/InputComponent.h>
#include <components/WindowHeader/WindowHeader.h>
#include <components/TextComponent/TextComponent.h>
#include <components/ErrorDiffuser/ErrorDiffuser.h>
#include "file-input-page.h"
#include "../../structures/circle.h"
#include "../../constants/lab3_constants.h"
#include "../../headers/lab3-headers.h"

Lab3FileInputPage::Lab3FileInputPage() { }

bool Lab3FileInputPage::display(
  std::vector<std::unique_ptr<Circle>>* circles,
  ui::WindowManager* window_manager
) {
  std::ifstream file;
  std::string file_name;
  double x, y, radius;
  int index = 0;
  std::function<std::string(std::string)> parse_double = [&file](std::string value) -> std::string {
    file.open(value);

    if (!file.is_open()) {
      ui::ErrorMessage("File not found").display();
      console::clear_lines_before(3);

      throw std::runtime_error("File not found");
    }

    return value;
  };

  circles->clear();
  ui::WindowHeader(headers::lab3::file_input_header).display();
  ui::TextComponent prompt_text("Enter file name: ");
  ui::InputReaderComponent<std::string> prompt(file_name, parse_double);
  ui::ErrorDiffuser({ &prompt_text, &prompt }).display();
  ui::TextComponent("Read information from " + file_name + "...\n\n").display();

  while (file >> x >> y >> radius) {
    std::unique_ptr<Circle> result = std::make_unique<Circle>(Circle { x, y, radius });
    circles->push_back(std::move(result));
  }

  ui::TextComponent("Informations was read. Count of received: " + std::to_string(circles->size())).display();

  ui::InputComponent([](std::string _) -> bool { return false; }).display();
  file.close();
  return !window_manager->switch_to(pages::lab3::input_ask);
}
