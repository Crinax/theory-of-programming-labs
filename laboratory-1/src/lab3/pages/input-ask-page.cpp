#include <core/ui.h>
#include <components/WindowHeader/WindowHeader.h>
#include <components/Menu/Menu.h>
#include "input-ask-page.h"
#include "../../constants/lab3_constants.h"
#include "../../headers/lab3-headers.h"

Lab3InputAskPage::Lab3InputAskPage() {}

bool Lab3InputAskPage::display(ui::WindowManager* window_manager) {
  ui::WindowHeader(headers::lab3::input_ask_header).display();
  ui::Menu menu = { };

  menu.add("Read information from file", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab3::file_input);
  });

  menu.add("Input information from keyboard", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab3::keyboard_input);
  });

  menu.add("Back", [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab3::main);
  });

  menu.add("Exit", []() -> bool { return true; });

  return menu.display();
}
