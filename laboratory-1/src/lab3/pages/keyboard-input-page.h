#pragma once

#include <memory>
#include <vector>
#include <core/ui.h>
#include "../../structures/circle.h"

class Lab3KeyboardInputPage {
  public:
    Lab3KeyboardInputPage();

    bool display(std::vector<std::unique_ptr<Circle>>*, ui::WindowManager*);
};
