#pragma once

#include <core/ui.h>

class Lab3InputAskPage {
  public:
    Lab3InputAskPage();

    bool display(ui::WindowManager*);
};
