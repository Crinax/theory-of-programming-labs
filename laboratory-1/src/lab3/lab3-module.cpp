#include <functional>
#include <core/ui.h>
#include "../constants/lab3_constants.h"
#include "../helpers/sort/sort.h"
#include "lab3-module.h"
#include "pages/main-page.h"
#include "pages/input-ask-page.h"
#include "pages/file-input-page.h"

void Lab3Module::load_dependencies(ui::WindowManager* window_manager) {
  Lab3MainPage main_page = { };
  Lab3InputAskPage input_ask_page = { };
  Lab3FileInputPage file_input_page = { };

  std::function<bool(ui::WindowManager*)> main_view = std::bind(
    &Lab3MainPage::display,
    main_page,
    std::placeholders::_1
  );

  std::function<bool(ui::WindowManager*)> input_ask_view = std::bind(
    &Lab3InputAskPage::display,
    input_ask_page,
    std::placeholders::_1
  );

  std::function<bool(ui::WindowManager*)> file_input_view = std::bind(
    &Lab3FileInputPage::display,
    file_input_page,
    &this->circles,
    std::placeholders::_1
  );
  
  window_manager->add(pages::lab3::main, main_view);
  window_manager->add(pages::lab3::input_ask, input_ask_view);
  window_manager->add(pages::lab3::file_input, file_input_view);
}
