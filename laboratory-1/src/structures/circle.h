#pragma once

enum class CircleField {
  x,
  y,
  radius,
};

struct Circle {
  double x;
  double y;
  double radius;

  double get_value(CircleField field) {
    switch (field) {
      case CircleField::x: {
        return this->x;
      };
      case CircleField::y: {
        return this->y;
      };
      case CircleField::radius: {
        return this->radius;
      };
    }
  }
};
