#pragma once
#include <string>

namespace pages {
  namespace lab2 {
    const std::string main = "lab-2 -> main";
    const std::string input_values = "lab-2 -> input values";
    const std::string find_max_values = "lab-2 -> find max values";
    const std::string sort_values = "lab-2 -> sort values";
  }
}
