#pragma once

#include <string>

namespace pages {
  namespace lab3 {
    const std::string main = "lab-3 -> main";
    const std::string input_ask = "lab-3 -> input-ask";
    const std::string keyboard_input = "lab-3 -> keyboard-input";
    const std::string file_input = "lab-3 -> file-input";
    const std::string sorting = "lab-3 -> sorting";
  }
}
