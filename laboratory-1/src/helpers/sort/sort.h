#pragma once

#include <functional>
#include <memory>
#include <vector>
#include "../../structures/circle.h"

namespace helpers {
  template<typename T>
  std::unique_ptr<std::vector<T>> make_unique_vector() {
    std::unique_ptr<std::vector<T>> result =
      std::make_unique<std::vector<T>>();

    return std::move(result);
  }

  namespace sort {
    enum class SortMethod {
      quick,
      internal,
    };

    template<typename T>
    std::unique_ptr<std::vector<T>> quick_sort(
      std::unique_ptr<std::vector<T>> vec
    ) {
      if (vec->size() < 2) {
        return std::move(vec);
      }

      int pivot_index = 0;

      auto left = make_unique_vector<int>();
      auto right = make_unique_vector<int>();
      auto result = make_unique_vector<int>();
      
      T pivot = vec->at(pivot_index);
      for (int index = 1; index < vec->size(); index++) {
        T current_element = vec->at(index);
        if (pivot > current_element) {
          left->push_back(current_element);
        } else if (pivot < current_element) {
          right->push_back(current_element);
        }
      }

      left = quick_sort(std::move(left));
      right = quick_sort(std::move(right));
      
      result->insert(
        result->end(),
        std::make_move_iterator(left->begin()),
        std::make_move_iterator(left->end())
      );

      result->insert(
        result->end(),
        pivot
      );
      
      result->insert(
        result->end(),
        std::make_move_iterator(right->begin()),
        std::make_move_iterator(right->end())
      );
      
      return std::move(result);
    }
  }
  namespace sort_direction {
    template<typename T>
    struct asc {
      bool operator() (const T& a, const T& b) const {
        return a < b;
      }
    };

    template<typename T>
    struct desc {
      bool operator() (const T& a, const T& b) const {
        return a > b;
      }
    };

    template<typename T>
    std::function<const bool(const T& a, const T& b)> ptr_asc(CircleField field) {
      return [field](const T& a, const T& b) -> const bool {
        return a->get_value(field) < b->get_value(field);
      };
    }

    template<typename T>
    std::function<const bool(const T& a, const T& b)> ptr_desc(CircleField field) {
      return [field](const T& a, const T& b) -> const bool {
        return a->get_value(field) > b->get_value(field);
      };
    }
  }
}

