#include <functional>
#include <core/app.h>
#include <core/ui.h>
#include "main-module.h"
#include "pages/main-page.h"
#include "../constants/constants.h"

void AppModule::load_dependencies(ui::WindowManager* window_manager) {
  AppPage app_main_page = {};
  
  std::function<bool(ui::WindowManager*)> app_main_page_func = std::bind(
    &AppPage::display,
    app_main_page,
    std::placeholders::_1
  );

  window_manager->add(pages::base, app_main_page_func);
}
