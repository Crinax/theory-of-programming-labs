#pragma once
#include <core/app.h>
#include <core/ui.h>

class AppPage {
  public:
    AppPage();

    bool display(ui::WindowManager*);
};
