#include <functional>
#include <core/app.h>
#include <core/ui.h>
#include <components/WindowHeader/WindowHeader.h>
#include <components/Menu/Menu.h>
#include "main-page.h"
#include "../../constants/lab1_constants.h"
#include "../../constants/lab2_constants.h"
#include "../../constants/lab3_constants.h"
#include "../../headers/headers.h"

AppPage::AppPage() {};

bool AppPage::display(ui::WindowManager* window_manager) {
  ui::WindowHeader(headers::base_header).display();
  ui::Menu menu = {};

  std::function<bool()> lab1_callback = [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab1::main);
  };

  std::function<bool()> lab2_callback = [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab2::main);
  };

  std::function<bool()> lab3_callback = [window_manager]() -> bool {
    return !window_manager->switch_to(pages::lab3::main);
  };
  
  menu.add("Laboratory work 1", lab1_callback);
  menu.add("Laboratory work 2", lab2_callback);
  menu.add("Laboratory work 3", lab3_callback);
  menu.add("Laboratory work 4", []() -> bool { return false; });
  menu.add("Exit", []() -> bool { return true; });
  
  return menu.display();
}
