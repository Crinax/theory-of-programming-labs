#pragma once
#include <core/app.h>
#include <core/ui.h>

class AppModule : public application::Module {
  public:
    void load_dependencies(ui::WindowManager*);
};
