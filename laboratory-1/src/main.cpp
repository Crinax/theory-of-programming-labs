#include <core/app.h>
#include <core/console.h>
#include "./main/main-module.h"
#include "./lab1/lab1-module.h"
#include "./lab2/lab2-module.h"
#include "./lab3/lab3-module.h"

int main() {
  console::clear();
	
  application::App app = {};
  
	AppModule app_module = {};
  Lab1Module lab1_module = {};
  Lab2Module lab2_module = {};
  Lab3Module lab3_module = {};

	app.load_module(&app_module);
  app.load_module(&lab1_module);
  app.load_module(&lab2_module);
  app.load_module(&lab3_module);
	
	return app.run();
}
