#!/bin/bash

CURRENT_DIR="$(pwd)"

if [ ! -d "${CURRENT_DIR}/template/build" ]; then
  mkdir "${CURRENT_DIR}/template/build"
fi

if [ ! -f "${CURRENT_DIR}/template/build/libLaboratoryTemplate.so" ]; then
  cd "${CURRENT_DIR}/template/build" || exit
  cmake .. || exit
  make || exit
fi

cd "${CURRENT_DIR}" || exit

if [ ! -d "${CURRENT_DIR}/build" ]; then
  mkdir "${CURRENT_DIR}/build"
fi

cd "${CURRENT_DIR}/build" || exit
cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=1 || exit
make || exit

if [ -f "${CURRENT_DIR}/compile_commands.json" ]; then
  rm "${CURRENT_DIR}/compile_commands.json"
fi

ln -s "${CURRENT_DIR}/build/compile_commands.json" "${CURRENT_DIR}/compile_commands.json"

if [ ! -x "${CURRENT_DIR}/build/Laboratory1" ]; then
  echo "Something went wrong"
fi

if [ "${1}" = "and-run" ]; then
  "${CURRENT_DIR}/build/Laboratory1"
fi
